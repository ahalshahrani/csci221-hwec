#include <fstream>
#include <iostream>
#include <string>
using namespace std;


class BITMAPFILEHEADER               /**** BMP file header structure i.e. BM file net size should be 14 bytes ****/
{
private:
	unsigned short bfType;           /* Magic number for file 2 bytes 2bytes */
	unsigned int   bfSize;           /* Size of file  4 bytes 4 bytes+ 2bytes = 6 byts*/
	unsigned short bfReserved1;      /* Reserved 2 bytes 6bytes + 2bytes = 8 bytes */
	unsigned short bfReserved2;      /* ... 2 bytes 8 bytes+ 2bytes =10 bytes */
	unsigned int   bfOffBits;        /* Offset to bitmap data 4 bytes 10 bytes+ 4 bytes =14 bytes*/
public:
	unsigned short GETbfType();
	unsigned long int   GETbfSize();
	int  ReadBmpFileHeader(ifstream&);
	int  WriteBmpFileHeader(ofstream&);
};

#  define BF_TYPE 0x4D42                   /* "MB" */

class  BITMAPINFOHEADER      /**** BMP file info structure i.e. DIB file net size should be 40 bytes ****/
{
private:
	unsigned int   biSize;            /* Size of info header 4 bytes */ 
	int            biWS;              /* Width of image 4 bytes  net 4+4 = 8 bytes*/
	int            biHS;              /* Height of image 4 bytes net 8+4 = 12 bytes*/
	unsigned short int biPlanes;      /* Number of color planes 2 bytes net 12+2 = 14 bytes*/
	unsigned short int biBitCount;    /* Number of bits per pixel 2 bytes net 14+2= 16 bytes*/
	unsigned int   biCompression;     /* Type of compression to use  4 bytes net 16+4= 20 bytes*/
	unsigned int   biSZ;              /* Size of image data 4 bytes net 20 + 4= 24 bytes*/
	int            biXPelsPerMeter;   /* X pixels per meter 4 bytes net 24+4= 28 bytes*/
	int            biYPelsPerMeter;   /* Y pixels per meter 4 bytes net 28+4 bytes = 32 bytes*/
	unsigned int   biClrUsed;         /* Number of colors used 4 bytes net 32 + 4 bytes= 36 bytes*/
	unsigned int   biClrImportant;    /* Number of important colors 4 bytes, net 36 + 4 bytes= 40 bytes*/
public:
	int            GETbiWS();
	int            GETbiHS();
	unsigned short int GETbiBitCount();
	int  ReadBmpInfoHeader(ifstream&);
	int  WriteBmpInfoHeader(ofstream&);
};

unsigned short BITMAPFILEHEADER::GETbfType() {
	return bfType;
}

unsigned long int   BITMAPFILEHEADER::GETbfSize() {
	return bfSize;
}

int  BITMAPFILEHEADER::ReadBmpFileHeader(ifstream& fp) {
	fp.read((char*)&bfType, sizeof(bfType));
	fp.read((char*)&bfSize, sizeof(bfSize));
	fp.read((char*)&bfReserved1, sizeof(bfReserved1));
	fp.read((char*)&bfReserved2, sizeof(bfReserved2));
	fp.read((char*)&bfOffBits, sizeof(bfOffBits));
	return 1;
}

int  BITMAPFILEHEADER::WriteBmpFileHeader(ofstream& fp) {
	fp.write((char*)&bfType, sizeof(bfType));
	fp.write((char*)&bfSize, sizeof(bfSize));
	fp.write((char*)&bfReserved1, sizeof(bfReserved1));
	fp.write((char*)&bfReserved2, sizeof(bfReserved2));
	fp.write((char*)&bfOffBits, sizeof(bfOffBits));
	return 1;
}
int  BITMAPINFOHEADER::GETbiWS() {
	return biWS;
}
int  BITMAPINFOHEADER::GETbiHS() {
	return biHS;
}
unsigned short int BITMAPINFOHEADER::GETbiBitCount() {
	return biBitCount;
}
int  BITMAPINFOHEADER::ReadBmpInfoHeader(ifstream& fp) {
	fp.read((char*)&biSize, sizeof(biSize));
	fp.read((char*)&biWS, sizeof(biWS));
	fp.read((char*)&biHS, sizeof(biHS));
	fp.read((char*)&biPlanes, sizeof(biPlanes));
	fp.read((char*)&biBitCount, sizeof(biBitCount));
	fp.read((char*)&biCompression, sizeof(biCompression));
	fp.read((char*)&biSZ, sizeof(biSZ));
	fp.read((char*)&biXPelsPerMeter, sizeof(biXPelsPerMeter));
	fp.read((char*)&biYPelsPerMeter, sizeof(biYPelsPerMeter));
	fp.read((char*)&biClrUsed, sizeof(biClrUsed));
	fp.read((char*)&biClrImportant, sizeof(biClrImportant));
	return 1;
}
int  BITMAPINFOHEADER::WriteBmpInfoHeader(ofstream& fp) {
	fp.write((char*)&biSize, sizeof(biSize));
	fp.write((char*)&biWS, sizeof(biWS));
	fp.write((char*)&biHS, sizeof(biHS));
	fp.write((char*)&biPlanes, sizeof(biPlanes));
	fp.write((char*)&biBitCount, sizeof(biBitCount));
	fp.write((char*)&biCompression, sizeof(biCompression));
	fp.write((char*)&biSZ, sizeof(biSZ));
	fp.write((char*)&biXPelsPerMeter, sizeof(biXPelsPerMeter));
	fp.write((char*)&biYPelsPerMeter, sizeof(biYPelsPerMeter));
	fp.write((char*)&biClrUsed, sizeof(biClrUsed));
	fp.write((char*)&biClrImportant, sizeof(biClrImportant));
	return 1;
}
char grayscale(char red, char green, char blue)
{
	return (red + blue + green) / 3;
}

char get_red( char red,  char green, char blue, int depth , int intensity)
{
	         if ((grayscale(red, green, blue) + depth * 2) > 255)
				{
				 return 255;
				}
              else
				{
					 red = grayscale(red, green, blue) + depth * 2;
					 return red;
				}
}

char get_green(char red, char green, char blue, int depth, int intensity)
{
	if ((grayscale(red, green, blue) + depth ) > 255)
	{
		return 255;
	}
	else
	{
		green = grayscale(red, green, blue) + depth;
		return green;
	}
}

char get_blue(char red, char green, char blue, int depth, int intensity)
{
	if ((grayscale(red, green, blue) - intensity) < 0)
	{
		return 0;
	}
	else
	{
		blue = grayscale(red, green, blue) - intensity;
		return blue;
	}

}

int main(int argc, char* argv[]) 
{
	BITMAPFILEHEADER bfh;
	BITMAPINFOHEADER bhd;


	ifstream fp1(argv[1], ios::in | ios::binary);
	
	if (!fp1.is_open()) {
		cout << "Usage: " << argv[0] << " <input_filename> <output_filename>" << endl;
		return 1;
	}

	ofstream fp2(argv[2], ios::out | ios::binary);
	if (!fp2.is_open()) {
		cout << "Usage: " << argv[0] << " <input_filename> <output_filename>" << endl;
		return 1;
	}

	int depth = 20;
	int intensity = 3;
	cin >> depth;
	cin >> intensity;

	int  success = 0;
	success = bfh.ReadBmpFileHeader(fp1);
	if (!success)
	{
		/* Couldn't read the file header - return NULL... */
		fp1.close();
		return -1;
	}

	cout << bfh.GETbfSize() << bfh.GETbfType();
	if (bfh.GETbfType() != BF_TYPE)  /* Check for BM reversed, ie MB... */
	{
		cout << "ID is: " << bfh.GETbfType() << " Should have been" << 'M' * 256 + 'B';
		cout << bfh.GETbfType() / 256 << " " << bfh.GETbfType() % 256 << endl;
		/* Not a bitmap file - return NULL... */
		fp1.close();
		return 1;
	}


	cout << "Image data Size: " << bfh.GETbfSize() << endl;

	success = 0;
	success = bhd.ReadBmpInfoHeader(fp1);
	if (!success)
	{
		/* Couldn't read the file header - return NULL... */
		fp2.close();
		return -1;
	}

	cout << "Image Width Size:  " << bhd.GETbiWS() << endl;
	cout << "Image Height Size:  " << bhd.GETbiHS() << endl;
	cout << "Bitcount: " << bhd.GETbiBitCount() << endl;

	bfh.WriteBmpFileHeader(fp2);
	bhd.WriteBmpInfoHeader(fp2);
	unsigned char r, g, b;
	for (int i = 0; i < bhd.GETbiWS(); i++)
		for (int j = 0; j < bhd.GETbiHS(); j++) 
		{
			fp1.read((char*)&b, 1);
			fp1.read((char*)&g, 1);
			fp1.read((char*)&r, 1);
			char red=get_red(b, g, r,depth, intensity);
		    char green = get_green(b, g, r, depth, intensity);
		    char blue = get_blue(b, g, r, depth, intensity);
			fp2.write((char*)&red, sizeof(char));
			fp2.write((char*)&green, sizeof(char));
			fp2.write((char*)&blue, sizeof(char));
		}
	fp1.close();
	fp2.close();
	return 0;
}

